import unittest
from selenium import webdriver

class Czlowiek():
    def __init__(self, imie, kolor_wlosow): # tak wygląda metoda do inicjalizacji, innymi słowy konstruktor
        # o konstruktorach porozmawiamy przy kolejnych lekcjach
        self.imie = imie
        self.kolor_wlosow = kolor_wlosow
        self.wiek = 0
    def urodziny(self): # metoda wywoływana podczas urodzin
        self.wiek = self.wiek + 1
    def przedstaw_sie(self): # metoda służąca do przedstawiania się przez danego człowieka
        print('Mam na imie', self.imie, 'i mam lat', self.wiek)


Ala = Czlowiek('Ala', 'czarne')
Ala.urodziny()
Adam = Czlowiek('Adam', 'blond')

Ala.urodziny()
Adam.urodziny()
# mija jeszcze więcej czasu i...
Ala.urodziny()
Adam.urodziny()
# w końcu Ala zapytana jest o przedstawienie się...
Ala.przedstaw_sie()
# a następnie Adam...
Adam.przedstaw_sie()

print(Ala.wiek)