import unittest
from selenium import webdriver
import time

class MainTests(unittest.TestCase):
   def test_demo_login(self):
       driver = webdriver.Chrome(executable_path=r"C:\TestFiles\chromedriver.exe")
       driver.get('https://demobank.jaktestowac.pl/logowanie_etap_1.html')
       title = driver.title
       print(f'Actual title: {title}')
       assert 'Demobank - Bankowość Internetowa - Logowanie' == title
       driver.quit()

   def test_demo_accounts(self):
       driver = webdriver.Chrome(executable_path=r"C:\TestFiles\chromedriver.exe")
       driver.get('https://demobank.jaktestowac.pl/konta.html')
       title = driver.title
       print(f'Actual title: {title}')
       assert 'Demobank - Bankowość Internetowa - Konta' == title
       driver.quit()

   def test_demo_pulpit(self):
       driver = webdriver.Chrome(executable_path=r"C:\TestFiles\chromedriver.exe")
       driver.get('https://demobank.jaktestowac.pl/pulpit.html')
       title = driver.title
       print(f'Actual title: {title}')
       assert 'Demobank - Bankowość Internetowa - Pulpit' == title
       driver.quit()

   def test_main_page(self):
       driver = webdriver.Chrome(executable_path=r"C:\TestFiles\chromedriver.exe")
       driver.get('https://demobank.jaktestowac.pl/logowanie_prod.html')
       title = driver.title
       print(f'Actual title: {title}')
       assert 'Demobank - Strona główna - Logowanie' == title
       driver.quit()

#    pass - zaślepka
class LoginPageTests(unittest.TestCase):
   @classmethod
   def setUpClass(self):
       self.driver = webdriver.Chrome(executable_path=r"C:\TestFiles\chromedriver.exe")
   @classmethod
   def tearDownClass(self):
       self.driver.quit()
   def test_exact_text_for_login_form_header(self):
       driver = self.driver
       url = 'https://demobank.jaktestowac.pl/logowanie_etap_1.html'
       driver.get(url)
       login_form_header_element = driver.find_element_by_xpath('//*[@id="login_form"]/h1')
       login_form_header_text = login_form_header_element.text
       self.assertEqual('Wersja demonstracyjna serwisu demobank', login_form_header_text,
                        f'Expected title differ from actual title for page url: {url}')

   def test_button_dalej_is_disabled_when_login_input_is_empty(self):
       driver = self.driver
       url = 'https://demobank.jaktestowac.pl/logowanie_etap_1.html'
       driver.get(url)
       login_form_input_element = driver.find_element_by_xpath('//*[@id="login_id"]')
       login_form_input_element.clear()

       login_next_button_element = driver.find_element_by_xpath('//*[@id="login_next"]')
       login_next_button_disabled = login_next_button_element.get_property('disabled')
       self.assertEqual(True, login_next_button_disabled,
                        f'Expected state of "dalej" button: True , differ from actual: {login_next_button_disabled} , for page url: {url}')

   def test_display_error_message_when_user_submit_less_than_8_signs(self):
       driver = self.driver
       url = 'https://demobank.jaktestowac.pl/logowanie_etap_1.html'
       driver.get(url)
       login_form_input_element = driver.find_element_by_xpath('//*[@id="login_id"]')
       login_text = '1234567'
       login_form_input_element.send_keys(login_text)
       hint_button_element = driver.find_element_by_xpath(
           '//*[@id="login_id_container"]//*[@class="i-hint-white tooltip widget-info"]')
       hint_button_element.click()
       warning_message_element = driver.find_element_by_xpath('//*[@class="error"]')
       warning_message_text = warning_message_element.text
       self.assertEqual('identyfikator ma min. 8 znaków', warning_message_text,
                        f'Expected warning message differ from actual one for url: {url}')

   def test_button_dalej_respond_when_enters_8_signs_id(self):
       driver = self.driver
       url = 'https://demobank.jaktestowac.pl/logowanie_etap_1.html'
       driver.get(url)
       login_form_input_element = driver.find_element_by_xpath('//*[@id="login_id"]')
       login_form_input_element.clear()
       login_text = '12345678'
       login_form_input_element.send_keys(login_text)
       login_next_button_element = driver.find_element_by_xpath('//*[@id="login_next"]')
       login_next_button_element.click()
       time.sleep(3)
       login_next_button_element = driver.find_element_by_xpath('//*[@id="login_next"]')
       new_login_button_text = login_next_button_element.text
       self.assertEqual('zaloguj się', new_login_button_text,
                        f'Expected login button text: zaloguj się , differ from actual {new_login_button_text}')

       # Teraz Ty 1

   def test_correct_popup_text(self):
       driver = self.driver
       url = 'https://demobank.jaktestowac.pl/logowanie_etap_1.html'
       driver.get(url)
       login_reminder_element = driver.find_element_by_xpath('//*[@id="ident_rem"]')
       login_reminder_element.click()
       time.sleep(3)
       popup_text_element = driver.find_element_by_xpath('//*[@class="shadowbox-content contact-popup"]/div/h2')
       popup_text_element_text = popup_text_element.text
       self.assertEqual('ta funkcja jest niedostępna', popup_text_element_text,
                        f'Expected login button text differ from actual: {popup_text_element_text}')
       # Teraz Ty 2

   def test_correct_login_from_login_etap2(self):
       driver = self.driver
       url = 'https://demobank.jaktestowac.pl/logowanie_etap_2.html'
       driver.get(url)
       # finding login input box and sending value
       login_input_element = driver.find_element_by_xpath('//*[@id="login_id"]')
       login_input_element.send_keys('kocur131')
       # finding login password box and sending value
       password_input_element = driver.find_element_by_xpath('//*[@id="login_password"]')
       password_input_element.send_keys('123456789')
       # finding button 'zaloguj'
       button_next_element = driver.find_element_by_xpath('//*[@id="login_next"]')
       button_next_element.click()
       time.sleep(3)
       # finding unique element to check if login was successful
       messages_element = driver.find_element_by_xpath('//*[@id="show_messages"]')
       messages_element_text = messages_element.text
       self.assertEqual('Brak wiadomości', messages_element_text,
                        f'Expected login button text differ from actual: {messages_element_text}')

